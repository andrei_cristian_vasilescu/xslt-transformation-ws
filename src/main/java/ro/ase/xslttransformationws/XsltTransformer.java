package ro.ase.xslttransformationws;

import org.springframework.stereotype.Component;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.StringWriter;

@Component
public class XsltTransformer {

    private static final String XSL_FILE = "courses2students.xsl";

    public String transform(String xml) throws TransformerException {

        StreamSource xsl = new StreamSource(new File(XSL_FILE));
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer(xsl);


        StringWriter writer = new StringWriter();
        ByteArrayInputStream inputStream = new ByteArrayInputStream(xml.getBytes());

        StreamSource input = new StreamSource(inputStream);
        StreamResult output = new StreamResult(writer);


        transformer.transform(input, output);
        return output.getWriter().toString();
    }
}
