package ro.ase.xslttransformationws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.transform.TransformerException;

@RestController
public class TransformationController {

    @Autowired
    private XsltTransformer xsltTransformer;

    @PostMapping(value = "/transform", consumes = "application/xml", produces = "application/xml")
    public String transform(@RequestBody String xml) {
        try {
            return xsltTransformer.transform(xml);
        } catch (TransformerException e) {
            e.printStackTrace();
            return e.getCause().getMessage();
        }
    }
}
