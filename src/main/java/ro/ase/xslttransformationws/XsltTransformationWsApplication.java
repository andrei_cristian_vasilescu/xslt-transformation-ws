package ro.ase.xslttransformationws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XsltTransformationWsApplication {

    public static void main(String[] args) {
        SpringApplication.run(XsltTransformationWsApplication.class, args);
    }

}
